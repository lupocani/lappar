<!---
Detta är tänkt att användas tillsammans med pandoc. Kör följande:
$ pandoc tillgång-till-hatt.md -s -f markdown -o tillgång-till-hatt.pdf
-->

# Överenskommelse för tillgång av nyckeltagg till ^

Taggmottagare kommer att ha tillgång till ^ via en nyckeltagg.
Taggmottagare åtar sig följande:

1. Hålla ^ i god ordning.

2. Inte föra oväsen eller dylikt i ^.

3. Hålla ordning på nyckeltaggen.

4. Lämna rummet om det kommer behövas till annat.

Taggutlämnare kan när som helst be att få tillbaka taggen.

\vfill{}

\noindent \begin{tabular}{l l l}
\rule{5cm}{.2pt} & \rule{5cm}{.2pt}   & \rule{2cm}{.2pt}\\
Taggutlämnare    &  Namnförtydligande & Datum \\
\\
\\
\hrulefill    & \hrulefill         & \hrulefill \\
Taggmottagare &  Namnförtydligande & Datum \\
\end{tabular}
